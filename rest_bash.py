import subprocess
import os
from subprocess import Popen, PIPE
from subprocess import check_output
from flask import Flask 
from flask_httpauth import HTTPBasicAuth
from werkzeug.security import generate_password_hash, check_password_hash

auth = HTTPBasicAuth()
@auth.verify_password
def verify_password(username,password):
    if username in users:
        return check_password_hash(users.get(username), password)
    return False

user = os.getenv('AUTH_USER')
pwd = os.getenv('AUTH_PASSWORD')

users = {
    user: generate_password_hash(pwd)
}

def call_script_hostname():
    session = Popen(['/bin/bash','hostname.sh'], stdout=PIPE, stderr=PIPE)
    stdout, stderr = session.communicate()
    if stderr:
        raise Exception("Error "+str(stderr))
    return stdout.decode('utf-8')

def call_script_server_ip():
    session = Popen(['/bin/bash','server_ip.sh'], stdout=PIPE, stderr=PIPE)
    stdout, stderr = session.communicate()
    if stderr:
        raise Exception("Error "+str(stderr))
    return stdout.decode('utf-8')

app = Flask(__name__)
@app.route('/hostname', methods=['GET',])
@auth.login_required
def route_hostname():
    return '<pre>'+call_script_hostname()+'</pre>'

@app.route('/server_ip', methods=['GET',])
@auth.login_required
def route_server_ip():
    return '<pre>'+call_script_server_ip()+'</pre>'

app.run(debug=True)